Small web application for service desk (issue logging system).

Libraries used:

1. Zend Framework 1.11 [http://framework.zend.com/downloads/latest#ZF1] (path: library/Zend/)
2. DomPDF [http://code.google.com/p/dompdf/] (path: library/dompdf/)
3. Tiny_Mce 3.9.4 [http://www.tinymce.com/download/download.php] (path: public/tiny_mce/)
