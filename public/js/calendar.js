$(document).ready(function() {
    $('#date_from, #date_to').datepicker({
        dateFormat: 'yy-mm-dd',
        inline: true
    });
});