$(document).ready(function() {

    $('#Close').click(function() {
        $('.mail_form').hide();
    });
    
});

function open_mail_form() {
    var form = $('.mail_form');
    if(form.is(':hidden')) {
        form.show();
    } else {
        form.hide();
    }
}

function send_mail(url) {
    var x = true;
    if($('#mail').val().length == 0) {
        x = false;
        alert('Please set e-mail address');
    }
    if(x) {
        var params = '&n='+$('#name').val()+'&m='+$('#mail').val()+'&u='+$('#subject').val()+'&o='+$('#content').val();
        $.get(
            url+'/search/pdf/'+$('#params').val()+params+'&s=1',
            function(data) {
                if(data == '0') {
                    alert("E-mail address is unreachable");
                } else {
                    alert("E-mail was successfully sent!");
                    $('.mail_form').hide();
                }
            }
        )
    }
}