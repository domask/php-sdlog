$(document).ready(function() {

    $('#show_online').click(function() {
        var d = $('.online_users');
        if(d.is(':hidden')) {
            d.show();
        } else {
            d.hide();
        }
    });

});

function start_polling(url, type, offset) {
    var categories = ['', 'Important', 'Emergency'];
    $.post(url+'/ajax/record', {o: offset} ,function(data){
        var l = data.length;
        if(l > 0) {
            var html = '';
            var st = '';
            for(var i = 0; i < l; i++) {
                var read = (data[i].read == 1) ? '': 'unread';
                var updated = null;
                if(type == 'full') {
                    updated = (data[i].date_updated == null) ? '' : '<strong>Updated: </strong>'+data[i].date_updated+'<br />';
                    if(data[i].category == 1 || data[i].category == 2) {
                        st = (data[i].category == 2) ? "<span class='_red'>Emergency record</span><br />" : '<span class="_green">Important record</span><br />';
                    } else {
                        st = '';
                    }
                    var full = '<div class="record"><div class="_left"><div class="record_author">'+st+'<strong>Started by: </strong>'; 
                    full += data[i].username+'<br /><strong>Created: </strong>'+data[i].date_created+'<br />'+updated+'</div></div>';
                    full += '<div class="_right"><div class="record_content"><div class="record_date"><strong>Title: </strong><a class="title '+read+'" href="../../record/'+data[i].id+'">'+data[i].title+'<a/></div><hr /><div class="record_text">'+data[i].content+'</div>';
                    full += '</div><div class="record_buttons"><a class="button1" href="../../record/'+data[i].id+'">Comments ('+data[i].comment_count+')</a></div></div></div>';
                    html += full;
                    if(i == l-1) {
                        $('.container').html('');
                    }
                } else {  
                    var last_poster = (data[i].last_poster.length > 0) ? data[i].last_poster : '-';
                    st = (data[i].category == 2) ? 'style="color: #980000"': 'style="color: #006600"';
                    var line = '<div  class="forum_line"><div class="forum_title">';
                    if(data[i].category == 1 || data[i].category == 2) {
                        line += '<span class="post_type"><a href="category/'+categories[data[i].category].toLowerCase()+'" '+st+'>'+categories[data[i].category]+'</a> : </span>';
                    }
                    line += '<a class="'+read+'" href="record/'+data[i].id+'">'+data[i].title+'</a></div>';
                    line += '<div class="forum_author">'+data[i].username+'</div><div class="forum_date">'+data[i].date_created+'</div>';
                    line += '<div class="forum_last">'+last_poster+'</div><div class="forum_counter">'+data[i].comment_count+'</div></div>';
                    html += line;
                    if(i == l-1) {
                        $('.forum_line:first').nextAll().remove();
                    }
                }
                if(i == l-1) {
                    $('#last_id').val(data[i].id);    
                }
                $('.container').append(html);
                
            }
        }
    }, 'JSON');
}

function check_online(url) {
    $.post(url+'/ajax/online', function(data) {
        var l = data.length;
        var html = '';
        if(l > 0) {
            for(var i = 0; i < l; i++) {
                html += '<div class="user_line"><span class="_green">● </span>'+data[i].name+'</div>';
            }
            $('.online_users > .user_line').remove();
            $('.online_users').append(html);
        }
    }, 'JSON');
}