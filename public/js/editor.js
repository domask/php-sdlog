$(document).ready(function() {
    

    
});

function editor(elem, width, height) {
    tinyMCE.init({
        theme : "advanced",
        mode: "exact",
        elements : elem,
        theme_advanced_toolbar_location : "top",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
        + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
        + "bullist,numlist,outdent,indent, separator,link, unlink, separator, sub,sup, charmap",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        height: height+'px',
        width: width+'px'
    }); 
}
