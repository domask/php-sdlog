<?php

class Log_Form_Login extends Zend_Form {
    
    public function init() {
        $this->setName("login");
             
        $username = new Zend_Form_Element_Text('username');
        $username->setFilters(array('StringTrim', 'StringToLower'))
                 ->setRequired(true)
                 ->setLabel('Username');
        
        $username->addDecorators(array(
            array('Label', array('tag' => 'div')),
            array('HtmlTag', array('tag' => 'div')),
        ));
        
        $password = new Zend_Form_Element_Password('password');
        $password->setFilters(array('StringTrim'))
                 ->setRequired(true)
                 ->setLabel('Password');
        
        $password->addDecorators(array('Errors',
            array('Label', array('tag' => 'div')),
            array('HtmlTag', array('tag' => 'div'))
        ));
        
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setRequired(false)
               ->setIgnore(true)
               ->setLabel('Login');
        
        $submit->addDecorators(array(
            array('HtmlTag', array('tag' => 'div'))
        ));
        
        $this->addElements(array($username, $password, $submit));
    }
}

?>
