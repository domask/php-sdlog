<?php

class Log_Form_Backup extends Zend_Form {
    
    public function init() {
        
        $this->setName('backup_form');
        
        $decorators = $this->addClass('filename_block');
           
        $filename = new Zend_Form_Element_Text('filename');
        $filename->setLabel('Filename')
                 ->setRequired(true)
                 ->setDecorators($decorators);
        
        $decorators = $this->addClass('backups_block');
        $backups = new Log_Form_Element_BackupMultiSelect('backups');
        $backups->setDecorators($decorators)
                ->setLabel('Backups');
        
        $submit = new Zend_Form_Element_Submit('Backup');
        $submit->setLabel('Create backup')
               ->setDecorators(array(
            array('viewHelper', array('tag' => 'div')),
            array('HtmlTag', array('tag' => 'div', 'class' => 'create_backup'))
        ));
        
        $delete = new Zend_Form_Element_Submit('Delete');
        $delete->setLabel('Delete backup')
               ->setDecorators(array(
            array('viewHelper', array('tag' => 'div')),
            array('HtmlTag', array('tag' => 'div', 'class' => 'delete_backup'))
        ));
        
        $this->addElements(array($filename, $submit, $backups, $delete));
    }
    
    private function addClass($class) {
        $decorators = array(
            array('ViewHelper', array('tag' => 'div')),
            array('Label', array('tag' => 'div')),
            'Errors',
            array('HtmlTag', array('tag' => 'div', 'class' => $class))
        );   
        return $decorators;
    }
    
}

?>
