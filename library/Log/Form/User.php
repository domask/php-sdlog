<?php

class Log_Form_User extends Zend_Form {
    
    public function init() {
        
    $decorators = array(
        array('ViewHelper', array('tag' => 'div')),
        array('Label', array('tag' => 'div')),
        'Errors',
        'HtmlTag'
    );    
    
    $this->setName('user');
    $filters = array('StripTags', 'StringTrim');
    
    $uid = new Zend_Form_Element_Hidden('uid');
    $uid->addFilter('Int')
        ->removeDecorator('Label')
        ->setDecorators(array(
           array('ViewHelper', array('tag' => 'div')) 
        ));
    
    $firstname = new Zend_Form_Element_Text('firstname');
    $firstname->setLabel('Firstname')
              ->addFilters($filters)
              ->setDecorators($decorators);
    
    $lastname = new Zend_Form_Element_Text('lastname');
    $lastname->setLabel('Lastname')
             ->addFilters($filters)
             ->setDecorators($decorators);
    
    $role = new Zend_Form_Element_Select('role');
    $role->setLabel('Role')
         ->setMultiOptions(array(1 => 'Administrator', 2 => 'Editor', 3 => 'Viewer'))
         ->setRequired(true)
         ->setDecorators($decorators);
    
    $username = new Zend_Form_Element_Text('username');
    $username->setLabel('Username')
             ->setRequired(true)
             ->addValidator('NotEmpty')
             ->addFilters($filters)
             ->setDecorators($decorators);
    
    $password1 = new Zend_Form_Element_Password('password1');
    $password1->setLabel('Password')
              ->setRequired(true)
              ->addValidator('NotEmpty')
              ->addFilters($filters)
              ->setDecorators($decorators);
    
    $password2 = new Zend_Form_Element_Password('password2');
    $password2->setLabel('Repeat password')
              ->setRequired(true)
              ->addValidator('NotEmpty')
              ->addFilters($filters)
              ->setDecorators($decorators);
    
    $submit = new Zend_Form_Element_Submit('Submit');
    $submit->setDecorators(array(
        array('viewHelper', array('tag' => 'div')),
        array('HtmlTag', array('tag' => 'div', 'class' => 'submit_button'))
    ));
    
    $this->addElements(array($uid, $firstname, $lastname, $username, $role, $password1, $password2, $submit));
    
    }
}

?>
