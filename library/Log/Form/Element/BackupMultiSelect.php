<?php

class Log_Form_Element_BackupMultiSelect extends Zend_Form_Element_Multiselect {
    
    public function init() {
        
        $path = APPLICATION_PATH.'/../backups';
        if(is_dir($path)) {
            if($handle = opendir($path)) {
                while(false !== ($file = readdir($handle))) {
                    if(!in_array($file, array('.', '..'))) {
                        $this->addMultiOption($file, $file);
                    }
                }
            }
        }
        
    }
    
}

?>
