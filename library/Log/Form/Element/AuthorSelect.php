<?php

class Log_Form_Element_AuthorSelect extends Zend_Form_Element_Select {
    
    public function init() {
        
        $this->addMultiOption(0, 'All');
        $umapper = new Log_Model_UserMapper();
        $users = $umapper->fetchAll();
        foreach($users as $user) {
            $this->addMultiOption($user->getId(), ($user->getFirstname().' '.$user->getLastname()));
        }
        
    }
    
}

?>
