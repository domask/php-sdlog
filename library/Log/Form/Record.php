<?php

class Log_Form_Record extends Zend_Form {
    
    public function init() {
        
        $filters = array('StripTags', 'StringTrim');
        $this->setName('record_form');
        
        $rid = new Zend_Form_Element_Hidden('rid');
        $rid->addFilter('Int');
        $rid->removeDecorator('Label')
            ->removeDecorator('HtmlTag');
        
        $title = new Zend_Form_Element_Text('title');
        $title->setLabel('Title')
              ->setRequired(true)
              ->addFilters($filters)
              ->addValidator('NotEmpty');
        
        $title->setDecorators(array(
            array('ViewHelper', array('tag' => 'div')),
            array('Label', array('tag' => 'div')),
            'HtmlTag',
            'Errors'
        ));
        
        $category = new Zend_Form_Element_Radio('category');
        $category->setLabel('Category');
        $category->addMultiOptions(array(
            '0' => 'Regular',
            '1' => 'Important',
            '2' => 'Emergency'))
                 ->setValue(0);

        $category->setDecorators(array(
            array('ViewHelper', array('tag' => 'div')),
            array('Label', array('tag' => 'div')),
            'HtmlTag',
            'Errors'
        ));
        
        $content = new Zend_Form_Element_Textarea('content');
        $content->setLabel('Text')
                ->setRequired(true)
                ->setFilters($filters)
                ->addValidator('NotEmpty');
        
        $content->setDecorators(array(
            array('ViewHelper', array('tag' => 'div')),
            'Label',
            'HtmlTag',
            'Errors'
        ));
        
        $post = new Zend_Form_Element_Submit('Submit');
        $post->setLabel('Post');
        $post->setDecorators(array(
            array('viewHelper', array('tag' => 'div')),
            array('HtmlTag', array('tag' => 'div', 'class' => 'submit_button'))
        ));        
        
        $this->addElements(array($rid, $title, $category, $content, $post));
        
    }
    
}

?>
