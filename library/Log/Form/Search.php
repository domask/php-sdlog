<?php

class Log_Form_Search extends Zend_Form {
    
    public function init() {
        
        $this->setName('search');
        
        $decorators = array(
            array('ViewHelper', array('tag' => 'div')),
            array('Label', array('tag' => 'div')),
            'Errors',
            'HtmlTag'
        );
        
        $keywords = new Zend_Form_Element_Text('keywords');
        $keywords->setLabel('Keywords')
                 ->addFilters(array('StripTags', 'StringTrim'))
                 ->setDecorators($decorators);
        
        $categories = new Zend_Form_Element_Radio('category');
        $categories->setLabel('Category')
                    ->setValue('0')
                    ->addMultiOptions(array('0' => 'All', '3' => 'Regular', '2' => 'Emergency', '1' => 'Important information'));
        
        $categories->setDecorators(array(
            array('ViewHelper', array('tag' => 'div')),
            array('Label', array('tag' => 'div')),
            'HtmlTag',
            'Errors'
        ));

        $author = new Log_Form_Element_AuthorSelect('authors');
        $author->setLabel('Author')
               ->setDecorators($decorators);

        
        $date_from = new Zend_Form_Element_Text('date_from');
        $date_from->setLabel('Date from')
                  ->setDecorators($decorators);  
        
        $date_to = new Zend_Form_Element_Text('date_to');
        $date_to->setLabel('Date to')
                ->setDecorators($decorators);
        
        $submit = new Zend_Form_Element_Submit('Submit');
        $submit->setDecorators(array(
            array('viewHelper', array('tag' => 'div')),
            array('HtmlTag', array('tag' => 'div', 'class' => 'submit_button'))
        ));
          
        $this->addElements(array($keywords, $categories, $author, $date_from, $date_to, $submit));       
    }
    
}

?>
