<?php

class Log_Form_Comment extends Zend_Form {
    
    public function init() {
        
        $this->setName('comment_form');
       
        $rid = new Zend_Form_Element_Hidden('rid');
        $rid->addFilter('Int')
            ->addDecorators(array(
                array('Label', array('tag' => '')),
                array('HtmlTag', array('tag' => 'div'))
            ));
        
        $cid = new Zend_Form_Element_Hidden('uid');
        $cid->addFilter('Int')
            ->addDecorators(array(
                array('Label', array('tag' => '')),
                array('HtmlTag', array('tag' => 'div'))
            ));
                
        
        $comment = new Zend_Form_Element_Textarea('comment');
        $comment->setRequired(true)
                //->setLabel('Leave comment...')
                ->addFilters(array('StripTags', 'StringTrim'))
                ->addValidator('NotEmpty');
        
        $comment->addDecorators(array(
            array('Label', array('tag' => '')),
            array('HtmlTag', array('tag' => 'div'))
        ));
        
        $submit = new Zend_Form_Element_Submit('Submit');
        $submit->setLabel('Post comment');
        $submit->setDecorators(array(
            array('viewHelper', array('tag' => 'div')),
            array('HtmlTag', array('tag' => 'div', 'class' => 'submit_button'))
        ));
        
        $this->addElements(array($rid, $cid, $comment, $submit));
            
    }
}

?>
