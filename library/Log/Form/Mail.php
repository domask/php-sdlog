<?php

class Log_Form_Mail extends Zend_Form {
   
    public function init() {
        
    $decorators = array(
        array('ViewHelper', array('tag' => 'div')),
        array('Label', array('tag' => 'div')),
        'Errors',
        'HtmlTag'
    );    
    
    $this->setName('mail_form');
    
    $name = new Zend_Form_Element_Text('name');
    $name->setLabel('Name:')
         ->setAttrib('readonly', 'readonly')
         ->setDecorators($decorators);
    
    $mail = new Zend_Form_Element_Text('mail');
    $mail->setLabel('E-mail:')
         ->addFilters(array('StripTags', 'StringTrim'))
         ->setDecorators($decorators);
    
    $subject = new Zend_Form_Element_Text('subject');
    $subject->setLabel('Subject:')
            ->setValue('Search results from Service Desk Online Log')
            ->addFilters(array('StripTags', 'StringTrim'))
            ->setDecorators($decorators);
    
    $content = new Zend_Form_Element_Textarea('content');
    $content->setLabel('Content:')
            ->setFilters(array('StripTags','StringTrim'))
            ->setDecorators($decorators);
    
    $submit = new Zend_Form_Element_Button('Submit');
    $submit->setAttrib('onclick', 'javascript: send_mail("'.$this->getView()->baseUrl().'")');
    $submit->setDecorators(array(
        array('viewHelper', array('tag' => 'div')),
        array('HtmlTag', array('tag' => 'div', 'class' => 'submit_button'))
    ));
    
    $close = new Zend_Form_Element_Button('Close');
    $close->setDecorators(array(
        array('viewHelper', array('tag' => 'div')),
        array('HtmlTag', array('tag' => 'div', 'class' => 'close_button'))
    ));
    
    $this->addElements(array($name, $mail, $subject, $content, $close, $submit));
    
    }    
    
}

?>
