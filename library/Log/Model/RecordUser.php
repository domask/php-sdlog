<?php


class Log_Model_RecordUser {
    
    private $_id;
    private $_recordId;
    private $_userId;
    private $_read;

    public function __construct(array $attributes = null) {
        if (is_array($attributes)) {
            $this->setOptions($attributes);
        }
    }

    public function  __get($name) {
        $method = 'get' . ucfirst($name);
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid guestbook property');
        }
        return $this->$method();
    }

    public function  __set($name, $value) {
        $method = 'set' . ucfirst($name);
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid guestbook property');
        }
        $this->$method($value);
    }    

    public function getId() {
        return $this->_id;
    }
    
    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }
    
    public function getUserId() {
        return $this->_userId;
    }
    
    public function setUserId($id) {
        $this->_userId = (int) $id;
        return $this;
    }    
    
    public function getRecordId() {
        return $this->_recordId;
    }
    
    public function setRecordId($id) {
        $this->_recordId = (int) $id;
        return $this;
    }
    
    public function getRead() {
        return $this->_read;
    }
    
    public function setRead($value) {
        $this->_read = (int) $value;
        return $this;
    }
}

?>