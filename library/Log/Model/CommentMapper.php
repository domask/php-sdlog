<?php

class Log_Model_CommentMapper {
    
    private $_dbTable;

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Log_Model_DbTable_Comment');
        }
        return $this->_dbTable;
    }

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
    
    public function save(Log_Model_Comment $comment) {
        $data = array(
            'id'        => $comment->getId(),
            'user_id'   => $comment->getUserId(),
            'record_id' => $comment->getRecordId(),
            'content'   => $comment->getContent(),
            'date_updated' => $comment->getDateUpdated(),
            'deleted'    => $comment->getDeleted()
        );
        if (null === ($id = $comment->getId())) {
            $data['date_created'] = date('Y-m-d H-i-s');
            $this->getDbTable()->insert($data);
            return $this->getDbTable()->getAdapter()->lastInsertId();
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        }
    }
    
    public function find($id) {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $comment = new Log_Model_Comment();
        $comment->setId($row->id)
                ->setUserId($row->user_id)
                ->setRecordId($row->record_id)
                ->setContent($row->content)
                ->setDateCreated($row->date_created)
                ->setDateUpdated($row->date_updated);
        return $comment;
    }

    public function fetchAll($where = null) {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach ($resultSet as $row) {
            $entry = new Log_Model_Comment();
            $entry->setId($row->id)
                ->setUserId($row->user_id)
                ->setRecordId($row->record_id)
                ->setContent($row->content)
                ->setDateCreated($row->date_created)
                ->setDateUpdated($row->date_updated);
            $entries[] = $entry;
        }
        return $entries;
    }    
    
    public function fetchAllWithDependencies($where = null, $order) {
        $resultSet = $this->getDbTable()->fetchAll($where, $order);
        $entries   = array();
        $rmapper = new Log_Model_RecordMapper();
        $now = strtotime(date("Y-m-d H:i:s"));
        foreach ($resultSet as $row) {
            $user = $row->findDependentRowset('Log_Model_DbTable_User');
            $user = $user->current();            
            $entry = new Log_Model_Comment();
//            $record_query = $rmapper->getDbTable()->select()
//                                    ->from($rmapper->getDbTable(), array('COUNT(id) as kiekis'))
//                                    ->where('user_id = ?',$row->user_id);
//            $record_count = $rmapper->getDbTable()->fetchRow($record_query);
//            $comment_query = $this->getDbTable()->select()
//                                    ->from($this->getDbTable(), array('COUNT(id) as kiekis'))
//                                    ->where('user_id = ?', $row->user_id);
//            $comment_count = $this->getDbTable()->fetchRow($comment_query);            
            $entry->setId($row->id)
                ->setUserId($row->user_id)
                ->setUserName($user->firstname.' '.$user->lastname)
//                ->setUserJoinDate(strftime("%Y-%m-%d", strtotime($user->date_created)))
//                ->setUserPostCount(($record_count->kiekis+$comment_count->kiekis))
                ->setRecordId($row->record_id)
                ->setContent($row->content)
                ->setDateCreated($row->date_created)
                ->setDateUpdated($row->date_updated);
            $online = 0;
            if($now-strtotime($user->date_logged) < 120) {
                $online = 1;
            }
            $entry->setUserOnline($online);
            $entries[] = $entry;
        }
        return $entries;        
    }
    
    public function delete($id) {
        $this->getDbTable()->delete('id = '.$id);
    }
    
    public function deleteWithWhere($where = null) {
        echo $where;
        $this->getDbTable()->delete($where);
    }
    
    public function setDeletedCommentsByRid($rid) {
        $data = array('deleted' => 1);
        $this->getDbTable()->update($data, array('record_id = ?' => $rid));
    }    
}

?>
