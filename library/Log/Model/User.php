<?php

class Log_Model_User {
    
    private $_id;
    private $_firstname;
    private $_lastname;
    private $_username;
    private $_password;
    private $_date_created;
    private $_date_logged;
    private $_role;
    private $_view_mode;
    
    public function __construct(array $attributes = null) {
        if (is_array($attributes)) {
            $this->setOptions($attributes);
        }
    }

    public function  __get($name) {
        $method = 'get' . ucfirst($name);
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid user property');
        }
        return $this->$method();
    }

    public function  __set($name, $value) {
        $method = 'set' . ucfirst($name);
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid user property');
        }
        $this->$method($value);
    }

    public function getId() {
        return $this->_id;
    }
    
    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }
    
    public function getFirstname() {
        return $this->_firstname;
    }
    
    public function setFirstname($value) {
        $this->_firstname = (string) $value;
        return $this;
    }
    
    public function getLastname() {
        return $this->_lastname;
    }
    
    public function setLastname($value) {
        $this->_lastname = (string) $value;
        return $this;
    }
    
    public function getUsername() {
        return $this->_username;
    }
    
    public function setUsername($value) {
        $this->_username = (string) $value;
        return $this;
    }
    
    public function getPassword() {
        return $this->_password;
    }
    
    public function setPassword($value) {
        $this->_password = (string) $value;
        return $this;
    }
    
    public function getDateCreated() {
        return $this->_date_created;
    }
    
    public function setDateCreated($date) {
        $this->_date_created = $date;
        return $this;
    }
    
    public function getDateLogged() {
        return $this->_date_logged;
    }
    
    public function setDateLogged($date) {
        $this->_date_logged = $date;
        return $this;
    }
    
    public function getRole() {
        return $this->_role;
    }
    
    public function setRole($value) {
        $this->_role = (int) $value;
        return $this;
    }
    
    public function getViewMode() {
        return $this->_view_mode;
    }
    
    public function setViewMode($mode) {
        $this->_view_mode = (int) $mode;
        return $this;
    }
}

?>