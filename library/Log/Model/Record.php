<?php

class Log_Model_Record {
    
    private $_id;
    private $_userId;
    private $_userName;
    private $_userJoinDate;
    private $_userPostCount;
    private $_title;
    private $_content;
    private $_category;
    private $_dateCreated;
    private $_dateUpdated;
    private $_commentCount;
    private $_lastPoster;
    private $_read;
    private $_deleted;
    private $_userOnline;
    private $_comments;
    
    public function __construct(array $attributes = null) {
        if (is_array($attributes)) {
            $this->setOptions($attributes);
        }
    }

    public function  __get($name) {
        $method = 'get' . ucfirst($name);
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid guestbook property');
        }
        return $this->$method();
    }

    public function  __set($name, $value) {
        $method = 'set' . ucfirst($name);
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid guestbook property');
        }
        $this->$method($value);
    }

    public function getId() {
        return $this->_id;
    }
    
    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }
    
    public function getUserId() {
        return $this->_userId;
    }
    
    public function setUserId($id) {
        $this->_userId = (int) $id;
        return $this;
    }
    
    public function getUserName() {
        return $this->_userName;
    }
    
    public function setUserName($value) {
        $this->_userName = (string) $value;
        return $this;
    }
     
    public function getTitle() {
        return $this->_title;
    }
    
    public function setTitle($value) {
        $this->_title = (string) $value;
        return $this;
    }
    
    public function getContent() {
        return $this->_content;
    }
    
    public function setContent($value) {
        $this->_content = (string) $value;
        return $this;
    }
    
    public function getDateCreated() {
        return $this->_dateCreated;
    }
    
    public function setDateCreated($date) {
        $this->_dateCreated = $date;
        return $this;
    }
    
    public function getDateUpdated() {
        return $this->_dateUpdated;
    }
    
    public function setDateUpdated($date) {
        $this->_dateUpdated = $date;
        return $this;
    }
    
    public function getCategory() {
        return $this->_category;
    }
    
    public function setCategory($value) {
        $this->_category = (int) $value;
        return $this;
    }
    
    public function getCommentCount() {
        return $this->_commentCount;
    }
    
    public function setCommentCount($value) {
        $this->_commentCount = (int) $value;
        return $this;
    }
    
    public function getLastPoster() {
        return $this->_lastPoster;
    }
    
    public function setLastPoster($value) {
        $this->_lastPoster = (string) $value;
        return $this;
    }
    
    public function getUserJoinDate() {
        return $this->_userJoinDate;
    }
    
    public function setUserJoinDate($date) {
        $this->_userJoinDate = $date;
        return $this;
    }
    
    public function getUserPostCount() {
        return $this->_userPostCount;
    }
    
    public function setUserPostCount($value) {
        $this->_userPostCount = (int) $value;
        return $this;
    }
    
    public function getRead() {
        return $this->_read;
    }
    
    public function setRead($value) {
        $this->_read = (int) $value;
        return $this;
    }    
    
    public function getDeleted() {
        return $this->_deleted;
    }
    
    public function setDeleted($value) {
        $this->_deleted = (int) $value;
        return $this;
    }
    
    public function getUserOnline() {
        return $this->_userOnline;
    }
    
    public function setUserOnline($value) {
        $this->_userOnline = (int) $value;
        return $this;
    }
    
    public function setComments(array $comments) {
        $this->_comments = $comments;
        return $this;
    }
    
    public function getComments() {
        return $this->_comments;
    }
}
?>
