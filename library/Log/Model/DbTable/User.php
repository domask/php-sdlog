<?php

class Log_Model_DbTable_User extends Zend_Db_Table_Abstract {

    protected $_name = 'user';

    protected $_referenceMap = array(
        'User' => array(
            'columns'       => array('id'),
            'refTableClass' => 'Log_Model_DbTable_Record',
            'refColumns'    => array('user_id')
        ),
        
        'Comment' => array(
            'columns'       => array('id'),
            'refTableClass' => 'Log_Model_DbTable_Comment',
            'refColumns'    => array('user_id')
        )
    );
  
}

?>
