<?php

class Log_Model_DbTable_Comment extends Zend_Db_Table_Abstract {
    
    protected $_name = 'comment';
    
    protected $_dependentTables = array('Log_Model_DbTable_Record', 'Log_Model_DbTable_User');
}

?>
