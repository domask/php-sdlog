<?php

class Log_Model_DbTable_Record extends Zend_Db_Table_Abstract {
    
    protected $_name = 'record';
    
    protected $_dependentTables = array('Log_Model_DbTable_User');
   
    protected $_referenceMap = array(
        'Comment' => array(
            'columns'       => array('id'),
            'refTableClass' => 'Log_Model_DbTable_Comment',
            'refColumns'    => array('user_id')
        )
    );
}
?>
