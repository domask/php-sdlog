<?php


class Log_Model_RecordMapper {

    private $_dbTable;

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Log_Model_DbTable_Record');
        }
        return $this->_dbTable;
    }

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
    
    public function save(Log_Model_Record $record) {
        $data = array(
            'id'            => $record->getId(),
            'user_id'       => $record->getUserId(),
            'title'         => $record->getTitle(),
            'content'       => $record->getContent(),
            'category'      => $record->getCategory(),
            'date_updated'  => $record->getDateUpdated(),
            'deleted'       => $record->getDeleted()
        );
        if (null === ($id = $record->getId())) {
            $data['date_created'] = date('Y-m-d H-i-s');   
            $this->getDbTable()->insert($data);
            return $this->getDbTable()->getAdapter()->lastInsertId();
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        }
    }
    
    public function find($id) {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $record = new Log_Model_Record();
        $record->setId($row->id)
                ->setUserId($row->user_id)
                ->setTitle($row->title)
                ->setContent($row->content)
                ->setCategory($row->category)
                ->setDateCreated($row->date_created)
                ->setDateUpdated($row->date_updated);
        return $record;
    }
    
    public function findWithDependencies($id) {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $record = new Log_Model_Record();
        $user = $row->findDependentRowset('Log_Model_DbTable_User');
        $user = $user->current();
        $now = strtotime(date("Y-m-d H:i:s"));
//        $record_query = $this->getDbTable()->select()
//                                    ->from($this->getDbTable(), array('COUNT(id) as kiekis'))
//                                    ->where('user_id = ?',$row->user_id);
//        $record_count = $this->getDbTable()->fetchRow($record_query);
//        $cmapper = new Log_Model_CommentMapper();
//        $comment_query = $cmapper->getDbTable()->select()
//                                    ->from($cmapper->getDbTable(), array('COUNT(id) as kiekis'))
//                                    ->where('user_id = ?', $row->user_id);
//        $comment_count = $cmapper->getDbTable()->fetchRow($comment_query);
        $record->setId($row->id)
                ->setUserId($row->user_id)
                ->setUserName($user->firstname.' '.$user->lastname)
//                ->setUserJoinDate(strftime("%Y-%m-%d", strtotime($user->date_created)))
//                ->setUserPostCount($record_count->kiekis+$comment_count->kiekis)
                ->setTitle($row->title)
                ->setContent($row->content)
                ->setDateCreated($row->date_created)
                ->setDateUpdated($row->date_updated);
        $online = 0;
        if($now-strtotime($user->date_logged) < 120) {
            $online = 1;
        }
        $record->setUserOnline($online);        
        return $record;       
    }

    public function fetchAll($where = null) {
        $resultSet = $this->getDbTable()->fetchAll($where);
        $entries   = array();
        foreach ($resultSet as $row) {
            $entry = new Log_Model_Record();
            $entry->setId($row->id)
                ->setUserId($row->user_id)
                ->setTitle($row->title)
                ->setContent($row->content)
                ->setDateCreated($row->date_created)
                ->setDateUpdated($row->date_updated);
            $entries[] = $entry;
        }
        return $entries;
    }
    
    public function delete($id) {
        $this->getDbTable()->delete('id = '.$id);
    }
    
    // $set -> whether author, category or dates are included in search
    // $comments -> fetch with comments or not.
    public function FetchAllForSearch($where_for_records = null, $where_for_comments = null, $order = null, $set, $with_comments) {
        $query = $this->getDbTable()->select()
                                    ->from($this->getDbTable(), array('id'))
                                    ->where($where_for_records);
        $records_id = $this->getDbTable()->fetchAll($query);
        $id_list = null;
        if(count($records_id) > 0) {
            $id_list = '(';
            foreach($records_id as $rid) {
                $id_list .= $rid->id.',';
            }
            $id_list = rtrim($id_list, ',');
            $id_list .= ')';
        }
        if(!empty($where_for_comments)) {
            $cmapper = new Log_Model_CommentMapper();
            if(!empty($id_list)) {
                if(!empty($set)) {
                    $where_for_comments .= ' AND record_id NOT IN '.$id_list;
                } else {
                    $where_for_comments .= ' AND record_id IN '.$id_list;
                }
            }
            $cquery = $cmapper->getDbTable()->select()->from($cmapper->getDbTable(), array('record_id'))
                                        ->where($where_for_comments);
            $records_id = $cmapper->getDbTable()->fetchAll($cquery);
            if(count($records_id) > 0) {
                if(empty($id_list)) {
                    $id_list = '(';
                } else {
                    $id_list = rtrim($id_list, ')');
                    $id_list .= ',';
                }
                foreach($records_id as $rid) {
                    $id_list .= $rid->record_id.',';
                } 
                $id_list = rtrim($id_list, ',');
                $id_list .= ')';
            }
        }
        if($with_comments) {
            return (!empty($id_list)) ? $this->FetchAllWithComments('id IN '.$id_list, $order) : array(); 
        } else {
            return (!empty($id_list)) ? $this->FetchAllWithDependencies('id IN '.$id_list, $order) : array();
        }
    }
    
    public function FetchAllWithDependencies($where = null, $order = null, $count = null, $offset = null) {
        $resultSet = $this->getDbTable()->fetchAll($where, $order, $count, $offset);
        $entries   = array();
        $cmapper = new Log_Model_CommentMapper();
        $rmapper = new Log_Model_RecordUserMapper();
        foreach ($resultSet as $row) {
            $user = $row->findDependentRowset('Log_Model_DbTable_User');
            $user = $user->current();
            $query = $cmapper->getDbTable()->select()
                                            ->from($cmapper->getDbTable(), array('COUNT(id) as kiekis, MAX(date_created) as date, (SELECT CONCAT(firstname," ",lastname) FROM user WHERE id = (SELECT user_id FROM comment WHERE record_id = "'.$row->id.'" AND date_created = (SELECT MAX(date_created) FROM comment WHERE record_id = "'.$row->id.'"))) as name'))
                                            ->where('deleted = 0 AND record_id = ?', $row->id);
            $comment = $cmapper->getDbTable()->fetchRow($query);
            $query_read= $rmapper->getDbTable()->select()
                                               ->where('record_id = "'.$row->id.'" AND user_id = "'.Zend_Auth::getInstance()->getIdentity()->id.'"');
            $read = $rmapper->getDbTable()->fetchRow($query_read);
            $read = (!empty($read)) ? $read->read : '0';
            $entry = new Log_Model_Record();
            $entry->setId($row->id)
                ->setTitle($row->title)
                ->setUserId($user->id)
                ->setUserName($user->firstname.' '.$user->lastname)
                ->setContent($row->content)
                ->setCategory($row->category)
                ->setDateCreated($row->date_created)
                ->setLastPoster($comment->name)
                ->setDateUpdated($comment->date)
                ->setCommentCount($comment->kiekis)
                ->setRead($read);
            $entries[] = $entry;
        }
        return $entries;
    }
    
    public function FetchAllWithComments($where = null, $order = null, $count = null) {
        $resultSet = $this->getDbTable()->fetchAll($where, $order, $count);
        $entries = array();
        $cmapper = new Log_Model_CommentMapper();
        $rmapper = new Log_Model_RecordUserMapper();
        foreach($resultSet as $row) {
            $user = $row->findDependentRowset('Log_Model_DbTable_User');
            $user = $user->current();
            $comments = $cmapper->fetchAllWithDependencies('record_id = '.$row->id.' AND deleted = 0', 'date_created ASC');
            $entry = new Log_Model_Record();
            $entry->setId($row->id)
                ->setTitle($row->title)
                ->setUserId($user->id)
                ->setUserName($user->firstname.' '.$user->lastname)
                ->setContent($row->content)
                ->setCategory($row->category)
                ->setDateCreated($row->date_created)
                ->setDateUpdated($row->date_updated)
                ->setComments($comments);
            $entries[] = $entry;
        }
        return $entries;
    }
    
    public function getNumberOfEmergencies() {
        $query = $this->getDbTable()->select()->from($this->getDbTable(), array('COUNT(id) as kiekis'))
                                     ->where('category = 2 AND deleted = 0 AND date_created > DATE_SUB(NOW(), INTERVAL 24 HOUR)');
        return $this->getDbTable()->fetchRow($query);
    }
    
}

?>
