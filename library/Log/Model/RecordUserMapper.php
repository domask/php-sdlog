<?php

class Log_Model_RecordUserMapper {

    private $_dbTable;

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Log_Model_DbTable_RecordUser');
        }
        return $this->_dbTable;
    }

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    } 

    public function save(Log_Model_RecordUser $recorduser) {
        $data = array(
            'id'            => $recorduser->getId(),
            'user_id'       => $recorduser->getUserId(),
            'record_id'     => $recorduser->getRecordId(),
            'read'          => $recorduser->getRead()
        );
        if (null === ($id = $recorduser->getId())) {
            $this->getDbTable()->insert($data);
            return $this->getDbTable()->getAdapter()->lastInsertId();
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        }
    }
    
    public function find($id) {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $recorduser = new Log_Model_RecordUser();
        $recorduser->setId($row->id)
                   ->setUserId($row->user_id)
                   ->setRecordId($row->record_id)
                   ->setRead($row->read);
        return $recorduser;
    }
    
    public function rowExists($id, $uid) {
        $query = $this->getDbTable()->select()->where('record_id = "'.$id.'" AND user_id = "'.$uid.'"');
        if(0 == count($result = $this->getDbTable()->fetchRow($query))) {
            return false;
        } else {
            return $result->id;
        }
    }


    public function fetchAll($where = null) {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach ($resultSet as $row) {
            $entry = new Log_Model_RecordUser();
            $entry->setId($row->id)
                   ->setUserId($row->user_id)
                   ->setRecordId($row->record_id)
                   ->setRead($row->read);
            $entries[] = $entry;
        }
        return $entries;
    }   
    
    public function updateRead($where = null) {
        $query = $this->getDbTable()->update(array('read' => 0),$where);
    }
    
    public function deleteWithWhere($where = null) {
        $this->getDbTable()->delete($where);
    }
    
}

?>