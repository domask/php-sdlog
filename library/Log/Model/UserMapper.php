<?php

class Log_Model_UserMapper {

    private $_dbTable;

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Log_Model_DbTable_User');
        }
        return $this->_dbTable;
    }

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function save(Log_Model_User $user) {
        $data = array(
            'id'                => $user->getId(),
            'username'          => $user->getUsername(),
            'password'          => $user->getPassword(),
            'firstname'         => $user->getFirstname(),
            'lastname'          => $user->getLastname(),
            'date_created'      => $user->getDateCreated(),
            'date_logged'       => $user->getDateLogged(),
            'role'              => $user->getRole(),
            'view_mode'         => $user->getViewMode()
        );
        if (null === ($id = $user->getId())) {
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        }
    }

    public function find($id) {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $user = new Log_Model_User();
        $user->setId($row->id)
             ->setUsername($row->username)
             ->setPassword($row->password)
             ->setFirstname($row->firstname)
             ->setLastname($row->lastname)
             ->setPassword($row->password)
             ->setDateCreated($row->date_created)
             ->setDateLogged($row->date_logged)
             ->setRole($row->role)
             ->setViewMode($row->view_mode);
        return $user;
    }

    public function fetchAll($where = null, $order = null) {
        $resultSet = $this->getDbTable()->fetchAll($where, $order);
        $entries   = array();
        foreach ($resultSet as $row) {
            $entry = new Log_Model_User();
            $entry->setId($row->id)
                  ->setUsername($row->username)
                   ->setPassword($row->password)
                   ->setFirstname($row->firstname)
                   ->setLastname($row->lastname)
                   ->setPassword($row->password)
                   ->setDateCreated($row->date_created)
                   ->setDateLogged($row->date_logged)
                   ->setRole($row->role)
                   ->setViewMode($row->view_mode);
            $entries[] = $entry;
        }
        return $entries;
    }   
    
    public function delete($id) {
        $this->getDbTable()->delete('id = '.$id);
    }
}

?>
