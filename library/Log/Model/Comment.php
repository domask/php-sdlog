<?php

class Log_Model_Comment {

    private $_id;
    private $_userId;
    private $_userName;
    private $_recordId;
    private $_content;
    private $_dateCreated;
    private $_dateUpdated;
    private $_userJoinDate;
    private $_userPostCount;
    private $_deleted;
    private $_userOnline;
    
    public function __construct(array $attributes = null) {
        if (is_array($attributes)) {
            $this->setOptions($attributes);
        }
    }

    public function  __get($name) {
        $method = 'get' . ucfirst($name);
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid guestbook property');
        }
        return $this->$method();
    }

    public function  __set($name, $value) {
        $method = 'set' . ucfirst($name);
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid guestbook property');
        }
        $this->$method($value);
    }

    public function getId() {
        return $this->_id;
    }
    
    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }
    
    public function getUserId() {
        return $this->_userId;
    }
    
    public function setUserId($id) {
        $this->_userId = (int) $id;
        return $this;
    }

    public function getUserName() {
        return $this->_userName;
    }
    
    public function setUserName($value) {
        $this->_userName = (string) $value;
        return $this;
    }
    
    public function getRecordId() {
        return $this->_recordId;
    }
    
    public function setRecordId($id) {
        $this->_recordId = (int) $id;
        return $this;
    }
    
    public function getContent() {
        return $this->_content;
    }
    
    public function setContent($value) {
        $this->_content = (string) $value;
        return $this;
    }

    public function getDateCreated() {
        return $this->_dateCreated;
    }
    
    public function setDateCreated($date) {
        $this->_dateCreated = $date;
        return $this;
    }  
    
    public function getDateUpdated() {
        return $this->_dateUpdated;
    }
    
    public function setDateUpdated($date) {
        $this->_dateUpdated = $date;
        return $this;
    }
    
    public function getUserJoinDate() {
        return $this->_userJoinDate;
    }
    
    public function setUserJoinDate($date) {
        $this->_userJoinDate = $date;
        return $this;
    }
    
    
    public function getUserPostCount() {
        return $this->_userPostCount;
    }
    
    public function setUserPostCount($value) {
        $this->_userPostCount = (int) $value;
        return $this;
    }    
    
    public function getDeleted() {
        return $this->_deleted;
    }
    
    public function setDeleted($value) {
        $this->_deleted = (int) $value;
        return $this;
    }
    
    public function getUserOnline() {
        return $this->_userOnline;
    }
    
    public function setUserOnline($value) {
        $this->_userOnline = (int) $value;
        return $this;
    }
}

?>
