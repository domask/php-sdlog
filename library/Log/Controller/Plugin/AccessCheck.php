<?php

class Log_Controller_Plugin_AccessCheck extends Zend_Controller_Plugin_Abstract {
        
    private $_auth;
    private $_acl;   
    
    public function __construct(Zend_Acl $acl, Zend_Auth $auth) {
        $this->_auth = $auth;
        $this->_acl = $acl;
    }
    
    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        $resource = $request->getControllerName();
        $action = $request->getActionName();
        
        if($this->_auth->hasIdentity()) {
            $identity = $this->_auth->getStorage()->read();
            $roles = array('1' => 'admin', '2' => 'editor', '3' => 'viewer');
            
            $role = ($identity->role) ? $roles[$identity->role] : $identity->role;
            if(!$this->_acl->isAllowed($role, $resource, $action)) {
                $request->setControllerName('auth')
                        ->setActionName('login');
            }
        } else {
            $request->setControllerName('auth')
                    ->setActionName('login');
        }
        
    }
          
}

?>
