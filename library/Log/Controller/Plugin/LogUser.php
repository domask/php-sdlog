<?php

class Log_Controller_Plugin_LogUser extends Zend_Controller_Plugin_Abstract {
    
    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        if(Zend_Auth::getInstance()->hasIdentity()) {
            $mapper = new Log_Model_UserMapper();
            $user = $mapper->find(Zend_Auth::getInstance()->getIdentity()->id);
            $user->setDateLogged(date("Y-m-d H-i-s"));
            $mapper->save($user);
        } 
    }
    
}

?>
