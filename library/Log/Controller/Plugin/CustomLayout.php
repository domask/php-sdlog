<?php

class Log_Controller_Plugin_CustomLayout extends Zend_Controller_Plugin_Abstract {
    
    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        if($request->getActionName() == 'login') {
            $layout = Zend_Layout::getMvcInstance();
            $layout->setLayout($request->getActionName());
        } 
    }
    
}

?>
