<?php

class AjaxController extends Zend_Controller_Action {
    
    public function preDispatch() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }
    
    public function recordAction() {
        $model = new Application_Model_IndexModel();
        $records_array = array();
        $offset = ($this->_getParam('o')) ? $this->_getParam('o') : 0;
        $records = $model->fetchAllRecords(null, 'date_created DESC', 10, $offset);
        foreach($records as $record) {
            array_push($records_array, array('id' => $record->getId(),
                                    'user_id' => $record->getUserId(),
                                    'title'   => $record->getTitle(),
                                    'username' => $record->getUserName(),
                                    'content'  => $record->getContent(),
                                    'category' => $record->getCategory(),
                                    'date_created' => $record->getDateCreated(),
                                    'last_poster'  => $record->getLastPoster(),
                                    'date_updated' => $record->getDateUpdated(),
                                    'comment_count' => $record->getCommentCount(),
                                    'read'          => $record->getRead()
            ));
        }
        echo Zend_Json::encode($records_array);
    }
    
    public function loggedAction() {       
        if($this->_getParam('uid')) {
            $id = $this->_getParam('uid');
            $mapper = new Log_Model_UserMapper();
            $user = $mapper->find($id);
            $user->setDateLogged(date("Y-m-d H-i-s"));
            $mapper->save($user);
        } else {
            echo 'false';
        }
    }
    
    public function onlineAction() {
        $model = new Application_Model_UserModel();
        $users_array = array();
        $users = $model->getUsers('date_logged > (NOW() - INTERVAL 2 MINUTE)');
        foreach($users as $user) {
            array_push($users_array, array(
                'name' => $user->getFirstname().' '.$user->getLastname()
                ));
        }
        echo Zend_Json::encode($users_array);
    }
    
}

?>