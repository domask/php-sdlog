<?php

class BackupController extends Zend_Controller_Action {

    public function preDispatch() {
        if(!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/');
        }
    }  
    
    public function indexAction() {      
        $this->view->headLink()->appendStylesheet($this->view->baseUrl().'/css/backup.css');        
        $path = APPLICATION_PATH.'/../backups/';
        $this->view->backup_form = new Log_Form_Backup();
        $this->view->backup_form->getElement('filename')->setValue('Log_Backup_'.date('Y-m-d').'.sql');
        if($this->_getParam('Backup')) {
            $filename = $this->_getParam('filename');
            $result = exec('mysqldump log --password=pass --user=root --single-transaction >'.$path.$filename, $output);
            if(!empty($ouput)) {
                echo $output;
            }
            $this->_redirect('backup');
        }
        if($this->_getParam('Delete')) {
            if($this->_getParam('backups')) {
                $backups = $this->_getParam('backups');
                foreach($backups as $backup) {
                    unlink($path.$backup);
                }
            }
            $this->_redirect('backup');
        }
    }
}

?>
