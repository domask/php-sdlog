<?php

class IndexController extends Zend_Controller_Action {
    
    protected $_model;
    
    public function init() {
        $this->_model = new Application_Model_IndexModel();
    }
    
    public function indexAction() {         
        $this->view->headScript()->appendFile($this->view->baseUrl().'/js/js.js');            
        $records = $this->_model->fetchAllRecords(null, 'date_created DESC');
        $this->view->last_id = $records[0]->id;
        $paginator = Zend_Paginator::factory($records);
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setItemCountPerPage('10');
        $this->view->records = $paginator;
        $umodel = new Application_Model_UserModel();
        $this->view->online_users = $umodel->getUsers('date_logged > (NOW() - INTERVAL 2 MINUTE)');
        $session = Zend_Session::namespaceGet('Zend_Auth');
        $this->view->uid = $session['storage']->id;
        $this->view->text_size = $session['storage']->text_size;
        $this->view->page = $this->_getParam('page');
        if($this->_getParam('view')) {
            $session['storage']->view_mode = 1;
            $this->_model->updateViewMode($session['storage']->id, 1);
            $this->view->view_type = 'full';
        } else {
            $session['storage']->view_mode = 0;
            $this->_model->updateViewMode($session['storage']->id, 0);
            $this->view->view_type = '';
        }        
    }
    
    public function categoryAction() {
            $categories = array('important', 'emergency');
            $categories_v = array('important' => 1, 'emergency' => 2);
            $category = $this->getRequest()->getParam('category');
            $category = (in_array($category, $categories)) ? 'category = "'.$categories_v[$category].'"' : '';
            $this->view->records = $this->_model->fetchAllRecords($category, 'date_created DESC');
    }
    
}

