<?php

class UserController extends Zend_Controller_Action {
    
    private $_form;
    private $_model;
    
    public function preDispatch() {
        if(!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/');
        }
    }
    
    public function init() {
        $this->view->headLink()->appendStylesheet($this->view->baseUrl().'/css/user.css');
        $this->_form = new Log_Form_User();
        $this->_model = new Application_Model_UserModel();
    }
    
    public function indexAction() {
        $this->view->users = $this->_model->getUsers();
    }
    
    public function addAction() {
        $this->view->user_form = $this->_form;
        $this->view->user_form->getElement('Submit')->setLabel('Create');
        if($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            if($this->_form->isValid($data) && $data['password1'] == $data['password2']) {
                $this->_model->save($data);
                $this->_redirect('/user');
            } else {
                $this->_form->populate($data);
            }
        }
    }

    public function editAction() {
        $this->view->user_form = $this->_form; 
        $this->view->user_form->getElement('Submit')->setLabel('Update');        
        $this->_form->getElement('password1')->removeValidator('NotEmpty')
                                             ->setRequired(false);
        $this->_form->getElement('password2')->removeValidator('NotEmpty')
                                             ->setRequired(false);
        if($this->getRequest()->getParam('_uid_')) {
            $id = $this->getRequest()->getParam('_uid_');
            if(!empty($id)) {
                $data = $this->_model->getUser($id);
                $data = $this->_model->toArray($data);
                if(!empty($data)) {
                    $this->_form->populate($data);
                }
            }
        } else {
            $data = $this->getRequest()->getParams();
            if($this->_form->isValid($data) && $data['password1'] == $data['password2']) {
                $this->_model->save($data);
                $this->_redirect('/user');
            } else {
                $this->_form->populate($data);
            }
        }
    }
    
    public function deleteAction() {
        if($this->getRequest()->getParam('_uid_')) {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $this->_model->delete($this->getRequest()->getParam('_uid_'));
            $this->_redirect('/user');
        }    
    }
    
    public function onlineAction() {
        $this->view->users = $this->_model->getUsers('date_logged > (NOW() - INTERVAL 2 MINUTE)');
    }

}

?>
