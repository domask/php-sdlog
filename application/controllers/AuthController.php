<?php

class AuthController extends Zend_Controller_Action {
    
    protected $_model;
    
    public function init() {
        $this->_model = new Application_Model_IndexModel();
    }    
    
    public function preDispatch() {
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout($this->_request->getActionName());        
    }
    
    public function loginAction() {
        $this->view->headLink()->setStylesheet($this->view->baseUrl().'/css/login.css');          
        $login_form = new Log_Form_Login();
        $this->view->login_form= $login_form;
        $request = $this->getRequest();
        if($request->isPost()) {
            if($login_form->isValid($request->getParams())) {
                if($this->logged($request->getParams())) {
                    $this->_model->log(Zend_Auth::getInstance()->getIdentity()->id);
                    $this->_redirect('/');
                }
            } else {
                $login_form->populate($request->getParams());
            }
        }
    }
    
    public function logoutAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if(Zend_Auth::getInstance()->hasIdentity()) {
            Zend_Auth::getInstance()->clearIdentity(); 
        }
        $this->_redirect('/');
    }
    
    
    protected function logged($params) {
        $adapter = $this->getAuthAdapter();
        $adapter->setIdentity($params['username']) 
                ->setCredential($params['password']);
        $result = Zend_Auth::getInstance()->authenticate($adapter);
        if($result->isValid()) {
            $user = $adapter->getResultRowObject();
            Zend_Auth::getInstance()->getStorage()->write($user);
            return true;
        }
        return false;
    }
    
    protected function getAuthAdapter() {    
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
        $authAdapter->setTableName('user')
            ->setIdentityColumn('username')
            ->setCredentialColumn('password')
            ->setCredentialTreatment('MD5(?)');
        return $authAdapter;
    }    
    
}

?>
