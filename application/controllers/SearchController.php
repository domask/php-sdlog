<?php

class SearchController extends Zend_Controller_Action {
    
    protected $_model;
    protected $_auth;
    
    public function preDispatch() {
        if(!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/');
        }
    }      
    
    public function init() {
        $this->_model = new Application_Model_SearchModel();
        $this->_auth = Zend_Auth::getInstance();        
    }
    
    public function indexAction() {       
        $this->view->headLink()->appendStylesheet($this->view->baseUrl().'/css/search.css');
        $this->view->headLink()->appendStylesheet($this->view->baseUrl().'/css/jquery-ui.custom.css'); 
        $this->view->headScript()->appendFile($this->view->baseUrl().'/js/jquery-ui.custom.js');    
        $this->view->headScript()->appendFile($this->view->baseUrl().'/js/calendar.js');
        $search_form = new Log_Form_Search();
        $this->view->search_form = $search_form;
        if($this->getRequest()->isPost()) {
            $url = $this->_model->generateUrl($this->getRequest()->getParams());
            $this->_redirect('search/results/?'.$url);
        }
    }
    
    public function resultsAction() {
        $this->view->headScript()->appendFile($this->view->baseUrl().'/js/search.js');
        $this->view->headLink()->appendStylesheet($this->view->baseUrl().'/css/search.css');        
        $model = new Application_Model_SearchModel();     
        $records = $model->getSearchResults($this->getRequest()->getParams());
        $this->view->kiekis = count($records);
        $paginator = Zend_Paginator::factory($records);
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setItemCountPerPage('5');
        $this->view->view_type = ($this->_getParam('view')) ? 'full' : '';
        $this->view->records = $paginator;     
        $this->view->params = substr($_SERVER['REQUEST_URI'],strpos($_SERVER['REQUEST_URI'], '?'));
        $this->view->mail_form = new Log_Form_Mail();
        $identity = Zend_Auth::getInstance()->getIdentity();
        $name = $identity->firstname.' '.$identity->lastname;
        $this->view->mail_form->getElement('name')->setValue($name);
        if($this->_getParam('k')) {
            $this->view->keywords = $this->_getParam('k');
        }
    }
    
    // Refactor code duplication
    public function last24Action() {
        $this->view->headScript()->appendFile($this->view->baseUrl().'/js/search.js');
        $this->view->headLink()->appendStylesheet($this->view->baseUrl().'/css/search.css'); 
        $model = new Application_Model_SearchModel();
        $records = $model->getLast24Results();
        $this->view->kiekis = count($records);
        $paginator = Zend_Paginator::factory($records);
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setItemCountPerPage('50');
        $this->view->view_type = ($this->_getParam('view')) ? 'full' : '';
        $this->view->records = $paginator;     
        $this->view->params = substr($_SERVER['REQUEST_URI'],strpos($_SERVER['REQUEST_URI'], '?'));
        $this->view->mail_form = new Log_Form_Mail();
        $identity = Zend_Auth::getInstance()->getIdentity();
        $name = $identity->firstname.' '.$identity->lastname;
        $this->view->mail_form->getElement('name')->setValue($name);
        if($this->_getParam('k')) {
            $this->view->keywords = $this->_getParam('k');
        }
    }
    
    public function pdfAction() {          
        $save = $this->_getParam('s');
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        require_once 'dompdf_config.inc.php'; 
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->pushAutoloader('DOMPDF_autoload');   
        $html = $this->_model->generateHTML($this->_request->getParams(), $save);  
        $dompdf = new DOMPDF();
        $dompdf->set_paper("a4","portrait");
        $dompdf->load_html($html);       
        $dompdf->set_base_path($_SERVER['DOCUMENT_ROOT']);
        $dompdf->render();        
        switch($save) {
            case('1'):
                $unique = uniqid();
                $path = APPLICATION_PATH.'/../public/temp/'.$unique.'.pdf';
                file_put_contents($path, $dompdf->output());
                $mail = new Zend_Mail();
                $transport = new Zend_Mail_Transport_Smtp('localhost');
                Zend_Mail::setDefaultTransport($transport);
                $mail->addTo($this->_getParam('m'))
                     ->setBodyText($this->_getParam('o'))
                     ->setFrom('info@sdol', $this->_getParam('n'))
                     ->setSubject($this->_getParam('u'));
                $at = new Zend_Mime_Part(file_get_contents($path));
                $at->type        = 'application/pdf';
                $at->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                $at->encoding = Zend_Mime::ENCODING_BASE64; 
                $at->filename    = $unique.'.pdf';
                $mail->addAttachment($at);
                try {
                    $mail->send();
                    unlink($path);
                } catch (Exception $e) {
                    echo 0;
                }
                break;
            case('0');
                $dompdf->stream("Log_search_results.pdf");                
                break;
            default:
                break;
        }
    }
    
}

?>