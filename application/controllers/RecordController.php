<?php


class RecordController extends Zend_Controller_Action {
    
    protected $_rmapper;
    protected $_model;
    protected $_auth;
    
    public function preDispatch() {
        if(!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/');
        }
    }    
    
    public function init() {
        $this->_rmapper = new Log_Model_RecordMapper();
        $this->_model = new Application_Model_RecordModel();
        $this->view->headLink()->setStylesheet($this->view->baseUrl().'/css/record.css'); 
        $this->view->headScript()->appendFile($this->view->baseUrl().'/tiny_mce/tiny_mce.js');   
        $this->view->headScript()->appendFile($this->view->baseUrl().'/js/editor.js');         
        $this->_auth = Zend_Auth::getInstance()->getIdentity();
    }
    
    public function indexAction() {
        $id = $this->getRequest()->getParam('id');
        if(!empty($id) && is_int((int)$id)) {
            $this->view->identity = $this->_auth;
            $this->view->record = $this->_model->findWithDependencies($id);
            $this->view->comments = $this->_model->fetchComments($id);
            $comment_form = new Log_Form_Comment();
            $comment_form->getElement('uid')->setValue($this->_auth->id);
            $comment_form->getElement('rid')->setValue($id);
            $this->view->comment_form = $comment_form;
            $this->_model->setPostRead($id, $this->_auth->id, '1');
            if($this->_getParam('k')) {
                $this->view->keywords = $this->_getParam('k');
            }            
            //////////////////////////////////////////////////////
            /////////////     Comment POST !     /////////////////
            //////////////////////////////////////////////////////
            if($this->getRequest()->isPost()) {
                $data = $this->getRequest()->getParams();
                if($comment_form->isValid($data)) {
                    $this->_model->saveComment($data);
                    $this->_model->setPostUnread($id, $this->_auth->id);
                    $this->_redirect('/record/'.$id);
                } else {
                    $comment_form->populate($data);
                }
            }
            //////////////////////////////////////////////////////
            //////////////////////////////////////////////////////
        } else {
            $this->_redirect('/');
        }
    }
    
    public function addAction() {       
        $record_form = new Log_Form_Record();
        $this->view->record_form = $record_form;
        if($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            if($record_form->isValid($data)) {
                $record = new Log_Model_Record();
                $record->setTitle($data['title'])
                       ->setContent($data['content'])
                       ->setCategory($data['category'])
                       ->setDateUpdated(date("Y-m-d H-i-s"))
                       ->setUserId($this->_auth->id)
                       ->setDeleted(0);
                $this->_rmapper->save($record);
                $this->_redirect('/');;
            } else {
                $record_form->populate($data);
            }
        }        
    }
    
    public function editAction() {   
        $record_form = new Log_Form_Record();
        $this->view->record_form = $record_form;  
        $this->view->id = $this->getRequest()->getParam('id');
        if($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            if($record_form->isValid($data)) {
                $record = $this->_rmapper->find($data['rid']);
                $record->setTitle($data['title'])
                       ->setContent($data['content'])
                       ->setCategory($data['category']);
                $this->_rmapper->save($record);
                $this->_redirect('/');
            } else {
                $record_form->populate($data);
            }        
        } else {
            $id = $this->getRequest()->getParam('id');
            $record = $this->_rmapper->find($id);
            if($record instanceof Log_Model_Record) {
                $data = array('rid' => $id,
                              'title' => $record->getTitle(),
                              'category'    => $record->getCategory(),
                              'content' => $record->getContent());
                $record_form->getElement('Submit')->setLabel('Update');
                $record_form->populate($data);
            }
        }
    }
    
    public function deleteAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);        
        $data = $this->_model->getRecord($this->_getParam('id'));
        if($data['user_id'] == $this->_auth->id || in_array($this->_auth->role, array('1','2'))) {
            $this->_model->fullDelete($data['rid']);
        }
        $this->_redirect('/');
    }
    
    public function commenteditAction() {       
        $this->view->id = $this->getRequest()->getParam('id');        
        $comment_form = new Log_Form_Comment();
        $comment_form->getElement('comment')->setLabel('');
        $this->view->comment_form = $comment_form;
        if($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            if($comment_form->isValid($data)) {
                $this->_model->saveComment($data);
                $this->_model->setPostUnread($data['rid'], $identity->id);
                $this->_redirect('/record/'.$data['rid']);
            } else {
                $comment_form->populate($data);
            }
        } else {
            $data = $this->_model->getComment($this->_getParam('cid'));
            $comment_form->getElement('uid')->setValue($data['user_id']);
            $comment_form->populate($data);
        }
    }
    
    public function commentdeleteAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $data = $this->_model->getComment($this->_getParam('cid'));
        if($data['user_id']== $this->_auth->id || in_array($this->_auth->role, array('1','2'))) {
            $this->_model->setCommentDeleted($data['cid']);
        }
        $this->_redirect('/record/'.$this->_getParam('id'));
    }
    
}

?>
