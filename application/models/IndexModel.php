<?php

class Application_Model_IndexModel {
    
    protected $_rmapper;


    public function __construct() {
        $this->_rmapper = new Log_Model_RecordMapper();
    }

    public function fetchAllRecords($where = null, $order = null, $count = null, $offset = null) {
        $where = (!empty($where)) ? $where.' AND deleted = 0' : 'deleted = 0';
        return $this->_rmapper->FetchAllWithDependencies($where, $order, $count, $offset);
    }
    
    public function delete($rid) {
        $this->_rmapper->delete($rid);
    }
    
    public function log($id) {
        $mapper = new Log_Model_UserMapper();
        $user = $mapper->find($id);
        $user->setDateLogged(date("Y-m-d H-i-s"));
        $mapper->save($user);
    }
    
    public function updateViewMode($uid, $mode) {
        $umapper = new Log_Model_UserMapper();
        $user = $umapper->find($uid);
        $user->setViewMode($mode);
        $umapper->save($user);
    }
 
}

?>
