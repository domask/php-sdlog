<?php


class Application_Model_UserModel {
    
    protected $_umodel;
    
    public function __construct() {
        $this->_umodel = new Log_Model_UserMapper();
    }
    
    public function save(array $data) {
        if(!empty($data['uid'])) {
            $user = $this->_umodel->find($data['uid']);
        } else {
            $user = new Log_Model_User();
        }
        $user->setFirstname($data['firstname'])
             ->setLastname($data['lastname'])
             ->setRole($data['role'])
             ->setUsername($data['username']);
        if(!empty($data['password1']) && !empty($data['password2']) && $data['password1'] == $data['password2']) {
            $user->setPassword(md5($data['password1']));
        }
        if(!empty($data['uid'])) {
            $user->setId($data['uid'])
                 ->setDateLogged(date("Y-m-d H-i-s"));
        } else {
            $user->setDateCreated(date("Y-m-d H-i-s"));
        }
        $this->_umodel->save($user);
    }
    
    public function getUsers($where = null) {
        return $this->_umodel->fetchAll($where, 'id ASC');
    }
    
    public function getUser($id) {
        return $this->_umodel->find($id);
    }
    
    public function toArray(Log_Model_User $user) {
        return $data = array(
            'uid'           => $user->getId(),
            'username'     => $user->getUsername(),
            'password'     => $user->getPassword(),
            'firstname'    => $user->getFirstname(),
            'lastname'     => $user->getLastname(),
            'date_created' => $user->getDateCreated(),
            'date_logged'  => $user->getDateLogged(),
            'role'         => $user->getRole()
        );
    }
    
    public function delete($id) {
        $this->_umodel->delete($id);
    }
}

?>
