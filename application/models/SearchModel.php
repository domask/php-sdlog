<?php

class Application_Model_SearchModel {
    
    protected $_rmapper;
    
    public function __construct() {
        $this->_rmapper = new Log_Model_RecordMapper();
    }
    
    // Main function for returning array with results;
    // $params = $_GET params from url;
    // $with_comments = fetch with comments or not? ( TRUE for pdf generation );
    public function getSearchResults($params, $with_comments = false) {
        $query_for_records = $this->createQuery($params, 1);
        $query_for_comments = (!empty($params['k'])) ? $this->createQuery($params, 2) : '';
        $set = (!empty($params['a']) && !empty($params['c']) && !empty($params['df']) && !empty($params['dt'])) ? true : false;
        return $this->_rmapper->FetchAllForSearch($query_for_records, $query_for_comments, 'date_created DESC', $set, $with_comments);
    }
    
    // Returns only records in the last 
    public function getLast24Results() {
        return $this->_rmapper->FetchAllForSearch('category = 2 AND deleted = 0 AND date_created > DATE_SUB(NOW(), INTERVAL 24 HOUR)', null, 'date_created DESC', false, false);
    }    
    
    // Generates url for PRG pattern;
    public function generateUrl($params) {
        $url = '';
        $url .= (!empty($params['authors'])) ? '&a='.$params['authors'] : '';
        $url .= (!empty($params['category'])) ? '&c='.$params['category'] : '';
        $url .= (!empty($params['date_from'])) ? '&df='.$params['date_from'] : '';
        $url .= (!empty($params['date_to'])) ? '&dt='.$params['date_to'] : '';
        if(!empty($params['keywords'])) {
            $keywords = str_replace(' ', '|', $params['keywords']);
            $url .= '&k='.$keywords;
        }
        return $url;
    }
    
    // Generates WHERE statement for SQL query. Takes parameters from url ($_GET)
    // $switch = 1 ( RECORD ) ; = 2 ( COMMENT );
    public function createQuery($params, $switch) {
        $where = 'deleted = 0 AND ';

        if($switch == 1) {
            $where .= (!empty($params['a'])) ? 'user_id = "'.$params['a'].'" AND ' : '';

            if(!empty($params['c']) && in_array($params['c'], array(1,2,3))) {
                $where .= 'category = ';
                $where .= ($params['c'] == '3') ?  '0' : $params['c'];
                $where .= ' AND ';
            }

            // GALIMA TIKRINT AR PILNA DATA
            if(!empty($params['df']) && empty($params['dt'])) {
                $where .= 'date_created >= "'.$params['df'].'" AND ';
            }

            if(empty($params['df']) && !empty($params['dt'])) {
                $where .= 'date_created <= "'.$params['dt'].'" AND ';
            }

            if(!empty($params['df']) && !empty($params['dt'])) {
                $where .= 'date_created BETWEEN "'.$params['df'].'" AND "'.$params['dt'].'" AND ';
            }
        }
        
        if(!empty($params['k'])) {
            $keywords = explode('|', $params['k']);
            $where .= '(';
            foreach($keywords as $keyword) {
                $keyword = strtolower($keyword);
                if($switch == 1) {
                    $where .= "LOWER(title) LIKE '%".$keyword."%' OR LOWER(content) LIKE '%".$keyword."%' OR ";
                } else {
                    $where .= "LOWER(content) LIKE '%".$keyword."%' OR ";
                }
            }
            $where = rtrim($where, 'OR ');
            $where .= ') AND ';
        }
        
        $where = rtrim($where, 'AND ');
        
        return $where;
    }
    
    
    // Generates HTML for PDF file.
    public function generateHTML($params, $switch) {      
        $html = "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body style='font-size:10px;'>";
        if(!empty($params['last24'])) { 
            $records = $this->_rmapper->FetchAllForSearch('category = 2 AND deleted = 0 AND date_created > DATE_SUB(NOW(), INTERVAL 24 HOUR)', null, 'date_created DESC', false, true);
        } else {
            $records = $this->getSearchResults($params, true);
        }
        foreach($records as $record) {
            $html .= '<div class="record">';
                $html .= '<div class="_left">';
                    $html .= '<div class="record_author">';
                        $html .= '<strong>Started By: </strong>'.$record->getUserName().'<br/>';
                        $html .= '<strong>Created: </strong>'.$record->getDateCreated().'<br />';
                        if((date('Y', strtotime($record->getDateUpdated())) != '-0001' && $record->getDateCreated() != $record->getDateUpdated())) {
                            $html .= '<strong>Updated: </strong>'.$record->getDateUpdated().'<br />';
                        }
                        $html .= '<strong>Comments: </strong>'.$record->getCommentCount();
                    $html .= '</div>';
                $html .= '</div>';
                $html .= '<div class="_right">';
                    $html .= '<div class="record_content">';
                        $html .= '<div class="record_date"><strong>Title: </strong>'.$record->getTitle().'</div>';
                        $html .= '<div class="record_text">'.strip_tags($record->getContent()).'</div>';
                    $html .= '</div>';
                $html .= '</div>';
            $html .= '</div>';
            $comments = $record->getComments();
            if(!empty($comments)) {
                $k = 0;
                $html .= '<hr style="height: 0px; border: 1px solid #CCC;"/>';
                foreach($comments as $comment) {
                    $html .= '<div class="comment" style="padding-left: 50px;">';
                        $html .= '<div class="_left">';
                            $html .= '<strong>Commented by: </strong>'.$comment->getUserName().'<br />';
                            $html .= '<strong>Created: </strong>'.$comment->getDateCreated().'<br />';
                            if(date('Y', strtotime($comment->getDateUpdated())) != '-0001' && $comment->getDateCreated() != $comment->getDateUpdated()) {
                                $html .= '<strong>Updated: </strong>'.$comment->getDateUpdated().'<br />';
                            }
                        $html .= '</div>';
                        $html .= '<div class="_right">';
                            $html .= '<div class="comment_content">'.strip_tags($comment->getContent()).'</div>';
                        $html .= '</div>';
                    $html .= '</div>';
                    $html .= ($k+1 == count($comments)) ? '' : '<hr style="height: 0px; border: 1px solid #CCC;"/>';
                    $k++;
                }
            }
            $html .= '<hr style="height: 0px; border: 1px solid black;" />';
        }
        $html .= "</body></html>";
        return $html;
    }
    
}

?>
