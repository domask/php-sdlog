<?php

class Application_Model_AclModel extends Zend_Acl {
    
    public function __construct() {
        
        $this->addResource('index')
             ->addResource('record')
             ->addResource('search')
             ->addResource('user')    
             ->addResource('backup')
             ->addResource('error')
             ->addResource('auth')
             ->addResource('ajax');
        
        $this->addRole('viewer')
             ->addRole('editor', 'viewer')
             ->addRole('admin','editor');
        
        $this->allow('viewer', 'error')
             ->allow('viewer', 'ajax')
             ->allow('viewer', 'ajax', 'record')
             ->allow('viewer', 'ajax', 'logged')
             ->allow('viewer', 'ajax', 'online')
             ->allow('viewer', 'index')
             ->allow('viewer', 'index', 'category')
             ->allow('viewer', 'auth', 'login')
             ->allow('viewer', 'auth', 'logout')
             ->allow('viewer', 'record')
             ->allow('viewer', 'record', 'add')
             ->allow('viewer', 'record', 'edit')
             ->allow('viewer', 'record', 'delete')
             ->allow('viewer', 'record', 'commentedit')
             ->allow('viewer', 'record', 'commentdelete')
             ->allow('viewer', 'search')
             ->allow('viewer', 'search', 'results')
             ->allow('viewer', 'search', 'pdf')
             ->allow('admin', 'backup')
             ->allow('admin', 'user')
             ->allow('admin', 'user', 'add')
             ->allow('admin', 'user', 'edit')
             ->allow('admin', 'user', 'delete');
    }
    
}

?>
