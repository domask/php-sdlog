<?php

class Application_Model_RecordModel {
    
    protected $_rmapper;
    protected $_cmapper;
    protected $_rumapper;

    public function __construct() {
        $this->_rmapper = new Log_Model_RecordMapper();
        $this->_cmapper = new Log_Model_CommentMapper();
        $this->_rumapper = new Log_Model_RecordUserMapper();
    }
    
    public function findWithDependencies($id) {
        return $this->_rmapper->findwithDependencies($id);
    }
    
    public function getRecord($rid) {
        $data = $this->_rmapper->find($rid);
        return array('rid' => $data->getId(),
                     'user_id' => $data->getUserId());
    }
    
    public function getComment($cid) {
        $data = $this->_cmapper->find($cid);
        return array('cid'=> $data->getId(), 
                     'user_id' => $data->getUserId(),
                     'comment' => $data->getContent(),
                     'rid' => $data->getRecordId());
    }
    
    public function saveComment(array $data) {
        $comment = new Log_Model_Comment();
        if(!empty($data['cid'])) {
            $comment->setId($data['cid']);
        }
        $comment->setRecordId($data['id'])
                ->setUserId($data['uid'])
                ->setContent($data['comment'])
                ->setDeleted(0);
        $this->_cmapper->save($comment);
        
        $record = $this->_rmapper->find($data['id']);
        $record->setDateUpdated(date("Y-m-d H-i-s"));
        $this->_rmapper->save($record);
    }
    
    public function fetchComments($rid) {
        return $this->_cmapper->fetchAllWithDependencies('record_id = "'.$rid.'" AND deleted = 0', 'date_created ASC');
    }
    
    public function deleteComment($cid) {
        $this->_cmapper->delete($cid);
    }
    
    public function setCommentDeleted($cid) {
        $comment = $this->_cmapper->find($cid);
        $comment->setDeleted('1');
        $this->_cmapper->save($comment);
    }
    
    public function setPostRead($rid, $uid, $value) {
        $ru = new Log_Model_RecordUser();
        $result = $this->_rumapper->rowExists($rid, $uid);
        if(!empty($result)) {
            $ru->setId($result);
        }
        $ru->setUserId($uid)
            ->setRecordId($rid)
            ->setRead($value);
        $this->_rumapper->save($ru);
    }
    
    public function setPostUnread($rid, $uid) {
        $this->_rumapper->updateRead('record_id = "'.$rid.'" AND user_id != "'.$uid.'"');
    }

    public function fullDelete($rid) {
        // sets record to deleted 1
        $record = $this->_rmapper->find($rid);
        $record->setDeleted('1');
        $this->_rmapper->save($record);
        
        // sets comments to deleted 1
        $this->_cmapper->setDeletedCommentsByRid($rid);
        
        // deletes records-comments relationships;
        $this->_rumapper->deleteWithWhere('record_id = "'.$rid.'"');
    }
}

?>
