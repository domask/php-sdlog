<?php

class Zend_View_Helper_IsActive extends Zend_View_Helper_Abstract {
    
    
    public function isActive($c, $a) {
        $name = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
        $action = Zend_Controller_Front::getInstance()->getRequest()->getActionName();
        return ($name == $c && $action == $a ) ? 'active' : ''; 
    }
    
}

?>
