<?php

class Zend_View_Helper_EmergencyCounter extends Zend_View_Helper_Abstract {
    
    public function emergencyCounter() {
        $mapper = new Log_Model_RecordMapper();
        $amount = $mapper->getNumberOfEmergencies();
        return $amount->kiekis;
    }
    
}

?>
