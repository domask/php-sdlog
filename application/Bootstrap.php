<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    
    protected function _initModels() {
        $resourceLoader = new Zend_Loader_Autoloader_Resource(array(
        'basePath'  => APPLICATION_PATH.'/models',
        'namespace' => 'models',
    ));
    }
    
    protected function _initPlugins() {
        //Zend_Controller_Front::getInstance()->registerPlugin(new Log_Controller_Plugin_CustomLayout());
        Zend_Controller_Front::getInstance()->registerPlugin(new Log_Controller_Plugin_LogUser());
        
        $acl = new Application_Model_AclModel();        
        Zend_Controller_Front::getInstance()->registerPlugin(new Log_Controller_Plugin_AccessCheck($acl, Zend_Auth::getInstance()));
    }
    
    protected function _initSession() {
        Zend_Session::start();
    }
    
    protected function _initRouters() {
        $router = Zend_Controller_Front::getInstance()->getRouter();
        
        $router->addRoute('Login', new Zend_Controller_Router_Route('/login',
                            array('controller' => 'auth', 'action' => 'login'))
        );
        
        $router->addRoute('Logout', new Zend_Controller_Router_Route('/logout',
                            array('controller' => 'auth', 'action' => 'logout'))
        );
        
        $router->addRoute('Record', new Zend_Controller_Router_Route('/record/:id',
                            array('controller' => 'record', 'action' => 'index'))
        );
        
        $router->addRoute('Record_Results', new Zend_Controller_Router_Route('/record/:id/keywords/:k',
                            array('controller' => 'record', 'action' => 'index'))
        );
        
        $router->addRoute('Record_Add', new Zend_Controller_Router_Route('/record/add',
                            array('controller' => 'record', 'action' => 'add'))
        );
        
        $router->addRoute('Record_Edit', new Zend_Controller_Router_Route('/record/:id/edit',
                            array('controller'  => 'record', 'action' => 'edit'))
        );
        
        $router->addRoute('Record_Delete', new Zend_Controller_Router_Route('/record/:id/delete',
                            array('controller' => 'record', 'action' => 'delete'))
        );
        
        $router->addRoute('Category', new Zend_Controller_Router_Route('/category/:category',
                            array('controller' => 'index', 'action' => 'category'))
        );
        
        $router->addRoute('Index_Pages', new Zend_Controller_Router_Route('/page/:page',
                            array('controller' => 'index', 'action' => 'index'))
        );
        
        $router->addRoute('View_Full', new Zend_Controller_Router_Route('/view/:view/',
                            array('controller' => 'index', 'action' => 'index'))
        );
        
        $router->addRoute('View_Full_Pages', new Zend_Controller_Router_Route('/view/:view/page/:page',
                            array('controller' => 'index', 'action' => 'index'))
        );
        
        $router->addRoute('Comment_Edit', new Zend_Controller_Router_Route('/record/:id/comment/:cid/edit',
                            array('controller' => 'record', 'action' => 'commentedit'))
        );
        
        $router->addRoute('Comment_Delete', new Zend_Controller_Router_Route('/record/:id/comment/:cid/delete',
                            array('controller' => 'record', 'action' => 'commentdelete'))
        );
        
        $router->addRoute('Search', new Zend_Controller_Router_Route('/search',
                            array('controller' => 'search', 'action' => 'index'))
        );
        
        $router->addRoute('Results', new Zend_Controller_Router_Route('/search/results/pdf/',
                            array('controller' => 'search', 'action' => 'pdf'))
        );
        
        $router->addRoute('Last24', new Zend_Controller_Router_Route('/search/results/last24',
                            array('controller' => 'search', 'action' => 'last24'))
        );
        
        $router->addRoute('User', new Zend_Controller_Router_Route('/user',
                            array('controller' => 'user', 'action' => 'index'))
        );
        
        $router->addRoute('Online', new Zend_Controller_Router_Route('/users',
                            array('controller' => 'user', 'action' => 'online'))
        );
        
    }

}

