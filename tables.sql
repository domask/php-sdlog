CREATE TABLE `log`.`user` (
  `id` SMALLINT  NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(20)  NOT NULL,
  `lastname` VARCHAR(30)  NOT NULL,
  `username` VARCHAR(6)  NOT NULL,
  `password` VARCHAR(32)  NOT NULL,
  `date_created` TIMESTAMP  NOT NULL,
  `date_logged` TIMESTAMP  NOT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = MyISAM
CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE `log`.`record` (
  `id` SMALLINT  NOT NULL AUTO_INCREMENT,
  `user_id` SMALLINT  NOT NULL,
  `title` VARCHAR(100)  NOT NULL,
  `content` TEXT  NOT NULL,
  `date_created` TIMESTAMP  NOT NULL,
  `date_updated` TIMESTAMP  NOT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = MyISAM
CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE `log`.`comment` (
  `id` SMALLINT(6)  NOT NULL AUTO_INCREMENT,
  `user_id` SMALLINT(6)  NOT NULL,
  `record_id` SMALLINT(6)  NOT NULL,
  `content` TEXT  NOT NULL,
  `date_created` TIMESTAMP  NOT NULL,
  `date_updated` TIMESTMAP ON UPDATE 
  PRIMARY KEY (`id`)
)
ENGINE = MyISAM
CHARACTER SET utf8 COLLATE utf8_unicode_ci;


CREATE TABLE `log`.`recorduser` (
  `id` SMALLINT(6)  NOT NULL AUTO_INCREMENT,
  `record_id` SMALLINT(6)  NOT NULL,
  `user_id` SMALLINT(6)  NOT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = MyISAM;
